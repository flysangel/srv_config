#!/bin/bash
clear
echo -e "\nWelcome to \"Micro Kube System\" (based on $(lsb_release -s -d))
The Documentation see <https://help.ubuntu.com>.\n"

# default shell
export SHELL='/bin/bash'

# network interface
export GW=$(ip route | awk '/default/ {print $3}')
export GWIF=$(ip route | awk '/default/ {print $5}')
export IP=$(ip a show "${GWIF}" | awk '/inet / {print $2}' | cut -d'/' -f1)
export MAC=$(ip a show "${GWIF}" | awk '/ether/ {print $2}')
export NETMASK=$(ip a show "${GWIF}" | awk '/inet / {print $2}' | cut -d'/' -f2)
case "${NETMASK}" in
24)
  NETID="$(echo "${IP}" | cut -d'.' -f1-3).0"
  ;;
16)
  NETID="$(echo "${IP}" | cut -d'.' -f1-2).0.0"
  ;;
8)
  NETID="$(echo "${IP}" | cut -d'.' -f1).0.0.0"
  ;;
*)
  NETID=''
  ;;
esac

if [ "${IP}" != "" ]; then
  echo "
${GWIF} IP: ${IP}/${NETMASK}
${GWIF} GW: ${GW}
"
fi
