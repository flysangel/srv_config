#!/bin/bash
if ! ls /root &>/dev/null; then
  echo 'Please use "sudo bash" run' && exit 1
fi

if ! grep -q 'Ubuntu.*LTS' /etc/os-release; then
  read -rp 'Ops, Not UBUNTU LTS, Press any key to continue.' && exit 1
fi

# update and install
curl -sL $(curl -sL https://api.github.com/repos/kubernetes-sigs/kind/releases/latest | jq -r .assets[].browser_download_url | grep 'linux-amd64$') -o kind
install -o root -g root -m 0755 kind /usr/local/bin/kind
rm -rf kind*

# $CMWHERE == null is vm or is docker
CMWHERE=$(grep -w 'docker\|libpod\|buildah' /proc/1/cgroup)
if [ "${CMWHERE}" == "" ]; then
  # kind network
  podman network exists kind || podman network create kind
fi
