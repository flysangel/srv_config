#!/bin/bash
if ! ls /root &>/dev/null; then
  echo 'Please use "sudo bash" run' && exit 1
fi

if ! grep -q 'Ubuntu.*LTS' /etc/os-release; then
  read -rp 'Ops, Not UBUNTU LTS, Press any key to continue.' && exit 1
fi

# update and install
curl -sLO "https://dl.k8s.io/release/$(curl -sL https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
rm -rf kubectl*

# env
tee /etc/profile.d/kubectl-env.sh <<EOF
source <(kubectl completion bash)
EOF
