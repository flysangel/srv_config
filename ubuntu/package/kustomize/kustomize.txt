#!/bin/bash
if ! ls /root &>/dev/null; then
  echo 'Please use "sudo bash" run' && exit 1
fi

if ! grep -q 'Ubuntu.*LTS' /etc/os-release; then
  read -rp 'Ops, Not UBUNTU LTS, Press any key to continue.' && exit 1
fi

# update and install
curl -sL $(curl -sL https://api.github.com/repos/kubernetes-sigs/kustomize/releases/latest | jq -r .assets[].browser_download_url | grep 'linux_amd64.tar.gz') -o kustomize.tar.gz
tar xf kustomize.tar.gz
install -o root -g root -m 0755 kustomize /usr/local/bin/kustomize
rm -rf kustomize*
