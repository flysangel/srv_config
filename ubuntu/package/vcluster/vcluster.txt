#!/bin/bash
if ! ls /root &>/dev/null; then
  echo 'Please use "sudo bash" run' && exit 1
fi

if ! grep -q 'Ubuntu.*LTS' /etc/os-release; then
  read -rp 'Ops, Not UBUNTU LTS, Press any key to continue.' && exit 1
fi

# update and install
curl -sL $(curl -sL https://api.github.com/repos/loft-sh/vcluster/releases/latest | jq -r .assets[].browser_download_url | grep 'vcluster-linux-amd64$') -o vcluster
install -o root -g root -m 0755 vcluster /usr/local/bin/vcluster
rm -rf vcluster*
