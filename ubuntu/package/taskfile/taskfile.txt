#!/bin/bash
if ! ls /root &>/dev/null; then
  echo 'Please use "sudo bash" run' && exit 1
fi

if ! grep -q 'Ubuntu.*LTS' /etc/os-release; then
  read -rp 'Ops, Not UBUNTU LTS, Press any key to continue.' && exit 1
fi

# download and install
curl -sL $(curl -sL https://api.github.com/repos/go-task/task/releases/latest | jq -r .assets[].browser_download_url | grep 'linux_amd64.tar.gz') -o task.tar.gz
tar xf task.tar.gz
install -o root -g root -m 0755 task /usr/local/bin/task
rm -rf task* completion LICENSE README.md

# completion bash
wget https://raw.githubusercontent.com/go-task/task/main/completion/bash/task.bash -qO /etc/profile.d/task-env.sh
