
# alpine package aconfig

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/alpine/package/aconfig/aconfig.txt | sudo bash
```

# alpine package docker

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/alpine/package/docker/docker.txt | sudo bash
```

# alpine package hadolint

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/alpine/package/hadolint/hadolint.txt | sudo bash
```

# alpine package helm

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/alpine/package/helm/helm.txt | sudo bash
```

# alpine package k8s

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/alpine/package/k8s/k8s.txt | sudo bash
```

# alpine package k8stools

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/alpine/package/k8stools/k8stools.txt | sudo bash
```

# alpine package kind

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/alpine/package/kind/kind.txt | sudo bash
```

# alpine package krew

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/alpine/package/krew/krew.txt | sudo bash
```

# alpine package kubectl

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/alpine/package/kubectl/kubectl.txt | sudo bash
```

# alpine package kubelinter

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/alpine/package/kubelinter/kubelinter.txt | sudo bash
```

# alpine package kubevirt

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/alpine/package/kubevirt/kubevirt.txt | sudo bash
```

# alpine package kustomize

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/alpine/package/kustomize/kustomize.txt | sudo bash
```

# alpine package mc

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/alpine/package/mc/mc.txt | sudo bash
```

# alpine package openjdk11

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/alpine/package/openjdk11/openjdk11.txt | sudo bash
```

# alpine package openjdk8

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/alpine/package/openjdk8/openjdk8.txt | sudo bash
```

# alpine package podman

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/alpine/package/podman/podman.txt | sudo bash
```

# alpine package taskfile

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/alpine/package/taskfile/taskfile.txt | sudo bash
```

# alpine package vcluster

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/alpine/package/vcluster/vcluster.txt | sudo bash
```

# alpine system base

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/alpine/system/base/base.txt | sudo bash
```

# alpine system bigred

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/alpine/system/bigred/bigred.txt | sudo bash
```

# alpine system disableSwap

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/alpine/system/disableSwap/disableSwap.txt | sudo bash
```

# alpine system enableSwap

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/alpine/system/enableSwap/enableSwap.txt | sudo bash
```

# alpine system ipv46

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/alpine/system/ipv46/ipv46.txt | sudo bash
```

# alpine system ntp

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/alpine/system/ntp/ntp.txt | sudo bash
```

# alpine system rclocal

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/alpine/system/rclocal/rclocal.txt | sudo bash
```

# alpine system sshenv

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/alpine/system/sshenv/sshenv.txt | sudo bash
```

# alpine system sudopw

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/alpine/system/sudopw/sudopw.txt | sudo bash
```

# ubuntu app mks

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/app/mks/mks.txt | sudo bash
```

# ubuntu novnc loginpw

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/novnc/loginpw/loginpw.txt | sudo bash
```

# ubuntu novnc screen

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/novnc/screen/screen.txt | sudo bash
```

# ubuntu package aconfig

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/package/aconfig/aconfig.txt | sudo bash
```

# ubuntu package chrome

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/package/chrome/chrome.txt | sudo bash
```

# ubuntu package docker

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/package/docker/docker.txt | sudo bash
```

# ubuntu package gitkraken

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/package/gitkraken/gitkraken.txt | sudo bash
```

# ubuntu package go

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/package/go/go.txt | sudo bash
```

# ubuntu package gpicview

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/package/gpicview/gpicview.txt | sudo bash
```

# ubuntu package hadolint

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/package/hadolint/hadolint.txt | sudo bash
```

# ubuntu package hadooplib

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/package/hadooplib/hadooplib.txt | sudo bash
```

# ubuntu package helm

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/package/helm/helm.txt | sudo bash
```

# ubuntu package intellij

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/package/intellij/intellij.txt | sudo bash
```

# ubuntu package k8s

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/package/k8s/k8s.txt | sudo bash
```

# ubuntu package k8stools

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/package/k8stools/k8stools.txt | sudo bash
```

# ubuntu package kind

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/package/kind/kind.txt | sudo bash
```

# ubuntu package krew

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/package/krew/krew.txt | sudo bash
```

# ubuntu package kubectl

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/package/kubectl/kubectl.txt | sudo bash
```

# ubuntu package kubelinter

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/package/kubelinter/kubelinter.txt | sudo bash
```

# ubuntu package kubevirt

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/package/kubevirt/kubevirt.txt | sudo bash
```

# ubuntu package kustomize

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/package/kustomize/kustomize.txt | sudo bash
```

# ubuntu package lens

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/package/lens/lens.txt | sudo bash
```

# ubuntu package libreoffice

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/package/libreoffice/libreoffice.txt | sudo bash
```

# ubuntu package lxqt

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/package/lxqt/lxqt.txt | sudo bash
```

# ubuntu package mc

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/package/mc/mc.txt | sudo bash
```

# ubuntu package openjdk11

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/package/openjdk11/openjdk11.txt | sudo bash
```

# ubuntu package openjdk8

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/package/openjdk8/openjdk8.txt | sudo bash
```

# ubuntu package podman

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/package/podman/podman.txt | sudo bash
```

# ubuntu package postman

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/package/postman/postman.txt | sudo bash
```

# ubuntu package python3

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/package/python3/python3.txt | sudo bash
```

# ubuntu package taskfile

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/package/taskfile/taskfile.txt | sudo bash
```

# ubuntu package vcluster

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/package/vcluster/vcluster.txt | sudo bash
```

# ubuntu package vlc

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/package/vlc/vlc.txt | sudo bash
```

# ubuntu package vscode

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/package/vscode/vscode.txt | sudo bash
```

# ubuntu package xfce

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/package/xfce/xfce.txt | sudo bash
```

# ubuntu system apt

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/system/apt/apt.txt | sudo bash
```

# ubuntu system base

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/system/base/base.txt | sudo bash
```

# ubuntu system bigred

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/system/bigred/bigred.txt | sudo bash
```

# ubuntu system disableCloudinit

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/system/disableCloudinit/disableCloudinit.txt | sudo bash
```

# ubuntu system disableSwap

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/system/disableSwap/disableSwap.txt | sudo bash
```

# ubuntu system enableSwap

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/system/enableSwap/enableSwap.txt | sudo bash
```

# ubuntu system ipv46

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/system/ipv46/ipv46.txt | sudo bash
```

# ubuntu system locale

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/system/locale/locale.txt | sudo bash
```

# ubuntu system ntp

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/system/ntp/ntp.txt | sudo bash
```

# ubuntu system rclocal

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/system/rclocal/rclocal.txt | sudo bash
```

# ubuntu system sshenv

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/system/sshenv/sshenv.txt | sudo bash
```

# ubuntu system sudopw

```bash
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/system/sudopw/sudopw.txt | sudo bash
```

