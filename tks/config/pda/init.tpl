# https://kubernetes.io/docs/reference/config-api/kubeadm-config.v1beta4/
apiVersion: kubeadm.k8s.io/v1beta4
kind: InitConfiguration
nodeRegistration:
  ignorePreflightErrors:
    - SystemVerification
  kubeletExtraArgs:
    - name: v
      value: "5"
---
apiVersion: kubeadm.k8s.io/v1beta4
kind: ClusterConfiguration
clusterName: "${CLUSTER_NAME}"
kubernetesVersion: "${KUBERNETES_VERSION}"
controlPlaneEndpoint: "${CONTROL_PLANE_ENDPOINT}:6443"
networking:
  dnsDomain: "${DNS_DOMAIN}"
  podSubnet: "${POD_SUBNET}"
  serviceSubnet: "${SERVICE_SUBNET}"
apiServer:
  extraArgs:
    - name: authorization-mode
      value: "${API_SERVER_AUTH_MODE}"
controllerManager:
  extraArgs:
    - name: node-cidr-mask-size
      value: "24"
imageRepository: "registry.k8s.io"
---
apiVersion: kubeproxy.config.k8s.io/v1alpha1
kind: KubeProxyConfiguration
mode: "${KUBE_PROXY_MODE}"
conntrack:
  maxPerCore: 0           # fix kube-proxy
ipvs:
  excludeCIDRs:
  - ${NETWORK_ID}         # kube-vip vip load balancer
---
apiVersion: kubelet.config.k8s.io/v1beta1
kind: KubeletConfiguration
maxPods: ${MAX_PODS}
authentication:
  anonymous:
    enabled: false
  webhook:
    enabled: true
  x509:
    clientCAFile: "/etc/kubernetes/pki/ca.crt"
authorization:
  mode: "Webhook"
