#!/bin/bash

set -o pipefail

clusterName="${1}"
if [[ -z "${clusterName}" ]]; then
  echo "Usage: $(basename "${0}") [cluster name]"
  exit 1
fi

baseDir=$(cd "$(dirname "${BASH_SOURCE[0]}")/../" && pwd)
configSetting="${baseDir}/config/${clusterName}/setting.conf"
if [[ ! -f "${configSetting}" ]]; then
  echo "Error: ${configSetting} was not found"
  exit 1
fi

# shellcheck source=/dev/null
source "${configSetting}"

declare -A itemCount
controlPlaneCount=0
workerCount=0
gpuWorkerCount=0

validate_network_type() {
  if [[ "${NETWORK_TYPE}" != "host" && "${NETWORK_TYPE}" != "bridge" && "${NETWORK_TYPE}" != "macvlan" ]]; then
    echo "Error: [NETWORK_TYPE] invalid value. Allowed values: host, bridge, macvlan"
    exit 1
  fi

  if [[ ("${NETWORK_TYPE}" == "bridge" || "${NETWORK_TYPE}" == "macvlan") && (-z "${NETWORK_ID}" || -z "${NETWORK_GW}") ]]; then
    echo "Error: [NETWORK_TYPE] is [${NETWORK_TYPE}]. [NETWORK_ID] and [NETWORK_GW] cannot be empty"
    exit 1
  fi
}

validate_required_vars() {
  missingVars=()
  [[ -z "${TK8S_LISTS}" ]] && missingVars+=("[TK8S_LISTS]")
  [[ -z "${NAME_SERVER}" ]] && missingVars+=("[NAME_SERVER]")
  [[ -z "${TK8S_IMAGE}" ]] && missingVars+=("[TK8S_IMAGE]")
  [[ -z "${CNI}" ]] && missingVars+=("[CNI]")
  [[ -z "${TAINT}" ]] && missingVars+=("[TAINT]")

  if [[ "${#missingVars[@]}" -gt 0 ]]; then
    echo "Error: The following required variables are missing: ${missingVars[*]}"
    exit 1
  fi
}

parse_container_info() {
  IFS='|' read -r ip account password k8sComponent _ containerName containerCpu containerRam containerIp <<<"${line}"

  if [[ -z "${ip}" || -z "${account}" || -z "${password}" || -z "${k8sComponent}" || -z "${containerName}" || -z "${containerCpu}" || -z "${containerRam}" ]]; then
    echo "Error: [TK8S_LISTS] is missing required fields"
    exit 1
  fi

  if [[ ("${NETWORK_TYPE}" == "bridge" || "${NETWORK_TYPE}" == "macvlan") && -z "${containerIp}" ]]; then
    echo "Error: [NETWORK_TYPE] is [${NETWORK_TYPE}]. The container IP cannot be empty"
    exit 1
  fi

  if [[ "${NETWORK_TYPE}" == "bridge" || "${NETWORK_TYPE}" == "macvlan" ]]; then
    if [[ -n "${itemCount["${containerIp}"]}" ]]; then
      echo "Error: [NETWORK_TYPE] is [${NETWORK_TYPE}]. The container IP [${containerIp}] must be unique"
      exit 1
    fi
    itemCount["${containerIp}"]=1
  fi

  if [[ "${NETWORK_TYPE}" == "host" ]]; then
    if [[ -n "${itemCount["${ip}"]}" ]]; then
      echo "Error: [NETWORK_TYPE] is [${NETWORK_TYPE}]. The IP address [${ip}] must be unique"
      exit 1
    fi
    itemCount["${ip}"]=1
  fi

  if [[ -n "${itemCount["${containerName}"]}" ]]; then
    echo "Error: The Container name [${containerName}] must be unique"
    exit 1
  fi
  itemCount["${containerName}"]=1
}

count_component() {
  case "${k8sComponent}" in
  control-plane) ((controlPlaneCount++)) ;;
  worker) ((workerCount++)) ;;
  gpu-worker) ((gpuWorkerCount++)) ;;
  *)
    echo "Error: [TK8S_LISTS] contains an invalid component: ${k8sComponent}. Allowed: control-plane, worker, gpu-worker"
    exit 1
    ;;
  esac
}

set_main_control_plane() {
  if [[ -z "${itemCount["${k8sComponent}"]}" && "${k8sComponent}" == "control-plane" ]]; then
    sed -i "s/^export.*MAIN_CONTROL_PLANE=.*/export MAIN_CONTROL_PLANE=\"${containerName}\"/g" "${configSetting}"
    sed -i "s/^export.*MAIN_IP=.*/export MAIN_IP=\"${ip}\"/g" "${configSetting}"
    sed -i "s/^export.*MAIN_ACCOUNT=.*/export MAIN_ACCOUNT=\"${account}\"/g" "${configSetting}"
    sed -i "s/^export.*MAIN_PASSWORD=.*/export MAIN_PASSWORD=\"${password}\"/g" "${configSetting}"
    itemCount["${k8sComponent}"]=1
  fi
}

check_component() {
  if [[ "${controlPlaneCount}" -eq 0 || "$((controlPlaneCount % 2))" -eq 0 ]]; then
    echo "Error: [TK8S_LISTS] must have an odd number of control-plane nodes (1, 3, 5, ...). Found: [${controlPlaneCount}]"
    exit 1
  fi

  if [[ "${workerCount}" -eq 0 && "${gpuWorkerCount}" -eq 0 ]]; then
    if [[ "${TAINT}" != "true" ]]; then
      echo "Error: [TK8S_LISTS] must have at least one worker or gpu-worker node. Found worker=[${workerCount}], gpu-worker=[${gpuWorkerCount}]"
      exit 1
    fi
  fi
}

# main
validate_network_type
validate_required_vars

IFS=$'\n' read -d '' -r -a TK8S_ARRAY <<<"${TK8S_LISTS}"
for line in "${TK8S_ARRAY[@]}"; do
  parse_container_info
  count_component
  set_main_control_plane
done

IFS=$'\n' read -d '' -r -a TK8S_ARRAY <<<"${TK8S_JOINS}"
for line in "${TK8S_ARRAY[@]}"; do
  parse_container_info
done

check_component
