#!/bin/bash

set -o pipefail

clusterName="${1}"
if [[ -z "${clusterName}" ]]; then
  echo "Usage: $(basename "${0}") [cluster name]"
  exit 1
fi

baseDir=$(cd "$(dirname "${BASH_SOURCE[0]}")/../" && pwd)
configSetting="${baseDir}/config/${clusterName}/setting.conf"
if [[ ! -f "${configSetting}" ]]; then
  echo "Error: ${configSetting} was not found"
  exit 1
fi

# shellcheck source=/dev/null
source "${configSetting}"

debug_mode() {
  if [[ "${DEBUG_MODE}" == "true" ]]; then
    echo -e "DEBUG: ${1} \ \n${2}"
  fi
}

parse_container_info() {
  IFS='|' read -r ip account password _ _ _ _ _ _ <<<"${line}"
  ssh=(sshpass -p "${password}" ssh "${SSH_OPTS[@]}" "${account}@${ip}")
}

check_ssh() {
  local sshCmd="\
  command -v podman 1>/dev/null"

  debug_mode "${ssh[*]}" "${sshCmd}"
  if ! "${ssh[@]}" "${sshCmd}"; then
    echo "Error: SSH connection to [${ip}] failed or Podman is not installed"
    exit 1
  fi
}

check_podman_cgroup() {
  local sshCmd="\
  sudo podman info -f json | jq -r .host.cgroupVersion | grep -q 'v2'"

  debug_mode "${ssh[*]}" "${sshCmd}"
  if ! "${ssh[@]}" "${sshCmd}"; then
    echo "Error: Podman cgroup on [${ip}] is not set to v2"
    exit 1
  fi
}

# main
IFS=$'\n' read -d '' -r -a TK8S_ARRAY <<<"${TK8S_LISTS}"
for line in "${TK8S_ARRAY[@]}"; do
  parse_container_info
  check_ssh
  check_podman_cgroup
done
