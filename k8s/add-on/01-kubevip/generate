#!/bin/bash
if ! sudo -l -U "$(whoami)" &>/dev/null; then
  echo "Sorry, user $(whoami) is not allowed to execute 'sudo' on $(hostname)" && exit 1
fi

baseDir=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)
yamlName=$(echo "${baseDir}" | awk -F'/' '{print $NF}')
dirName=$(echo "${yamlName}" | awk -F'-' '{print $NF}')
vip="192.168.0.1"
interface="eth0"
kvipVersion=$(curl -sL https://api.github.com/repos/kube-vip/kube-vip/releases/latest | jq -r .name)

sudo podman image pull ghcr.io/kube-vip/kube-vip:"${kvipVersion}"
sudo podman run --rm ghcr.io/kube-vip/kube-vip:"${kvipVersion}" manifest pod \
  --interface ${interface} \
  --address ${vip} \
  --arp \
  --controlplane \
  --leaderElection \
  --leaseDuration 15 \
  --leaseRenewDuration 10 \
  --leaseRetry 2 \
  --enableLoadBalancer |
  sed 's|imagePullPolicy: Always|imagePullPolicy: IfNotPresent|g' |
  tee "${baseDir}"/"${yamlName}".yaml &>/dev/null

sudo podman run --rm ghcr.io/kube-vip/kube-vip:"${kvipVersion}" manifest daemonset \
  --interface ${interface} \
  --arp \
  --services \
  --inCluster \
  --servicesElection \
  sed 's|imagePullPolicy: Always|imagePullPolicy: IfNotPresent|g' |
  tee "${baseDir}"/"${dirName}"-ds/"${dirName}"-ds.yaml &>/dev/null
kubectl kustomize "${baseDir}"/"${dirName}"-ds | tee "${baseDir}"/"${yamlName}"-ds.yaml &>/dev/null
