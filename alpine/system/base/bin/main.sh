#!/bin/bash
if ! ls /root &>/dev/null; then
  echo 'Please use "sudo bash" run' && exit 1
fi

if ! grep -q 'Alpine.*Linux' /etc/os-release; then
  read -rp 'Ops, Not Alpine Linux, Press any key to continue.' && exit 1
fi

# good time good start
sleep $((5 + RANDOM % 5))

# daemon
(. /mks/bin/daemon.sh) &
sleep 5

# enable dialog on alternative console
while true; do
  dialog --title 'Micro Kube System' --timeout 30 --textbox /mks/dialog/mks.dialog 24 80 &&
    clear
done
