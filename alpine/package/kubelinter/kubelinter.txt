#!/bin/bash
if ! ls /root &>/dev/null; then
  echo 'Please use "sudo bash" run' && exit 1
fi

if ! grep -q 'Alpine.*Linux' /etc/os-release; then
  read -rp 'Ops, Not Alpine Linux, Press any key to continue.' && exit 1
fi

# update and install
curl -sL $(curl -sL https://api.github.com/repos/stackrox/kube-linter/releases/latest | jq -r .assets[].browser_download_url | grep 'kube-linter-linux$') -o kube-linter
install -o root -g root -m 0755 kube-linter /usr/local/bin/kube-linter
rm -rf kube-linter*
